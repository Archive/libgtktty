#undef	ENABLE_NLS
#undef	HAVE_CATGETS
#undef	HAVE_GETTEXT
#undef	HAVE_LC_MESSAGES
#undef	HAVE_STPCPY
#undef	HAVE_WAIT4
#undef	HAVE_LIBSM

/* build with consideration of "linux" entry in termcap?
 */
#undef	HAVE_TERMCAP_LINUX

#undef	PACKAGE
#undef	LIBGTKTTY_MAJOR
#undef	LIBGTKTTY_REVISION
#undef	LIBGTKTTY_AGE
#undef	LIBGTKTTY_VERSION
#undef	VERSION

@BOTTOM@

/* general defines
 */
#define	LIBNAME		"LibGtkTty"
