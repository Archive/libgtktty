dnl
dnl Configure script for the GtkTty library
dnl
AC_INIT(libgtktty.h)
AM_CONFIG_HEADER(config.h)
LIBGTKTTY_MAJOR=0
LIBGTKTTY_REVISION=4
LIBGTKTTY_AGE=0
LIBGTKTTY_VERSION="$LIBGTKTTY_MAJOR.$LIBGTKTTY_REVISION.$LIBGTKTTY_AGE"
VERSION="$LIBGTKTTY_MAJOR.$LIBGTKTTY_REVISION"
AM_INIT_AUTOMAKE(libgtktty, [$VERSION])

dnl Support porgram name conversion.
AC_ARG_PROGRAM

dnl Disable automatic maintainer mode.
AM_MAINTAINER_MODE

dnl Make sure else configure stuff exists.
AC_CANONICAL_HOST

dnl Foreign defaults:
MC_IF_VAR_EQ(enable_static, yes, , enable_static=no)
MC_IF_VAR_EQ(enable_shared, no, , enable_shared=yes)

dnl Additional arguments.
AC_ARG_ENABLE(debug,
[  --enable-debug          turn on debugging [default=yes]],
,
dnl Default case:
enable_debug=yes)
AC_ARG_ENABLE(pedantic-ansi,
[  --enable-pedantic-ansi  turn on pedantic ansi compilation [default=no]],
,
dnl Default case:
enable_pedantic_ansi=no)
  
dnl Define package requirements.
AC_DEFUN(AC_LIBGTKTTY_REQUIREMENTS,
[
	AM_PATH_GTK(1.0.0,,AC_MSG_ERROR(Cannot find GTK: is gtk-config in path?))
])
AC_DEFUN(AC_GTKTERM_REQUIREMENTS,
[
])
AC_DEFUN(AC_GTKLED_REQUIREMENTS,
[
])
AC_DEFUN(AC_GTKVTEMU_REQUIREMENTS,
[
])
AC_DEFUN(AC_GTKVT102_REQUIREMENTS,
[
	dnl Check for 'linux' entry in termcap.
	AC_CACHE_CHECK(for terminal type \"linux\" in /etc/termcap,
	gemvt_cv_term_linux,
	if grep -i '^\([^      ]\+|\)*linux|' /etc/termcap >/dev/null 2>/dev/null; then
		gemvt_cv_term_linux="yes"
	else
		gemvt_cv_term_linux="no"
	fi)
	MC_IF_VAR_EQ(gemvt_cv_term_linux, yes, AC_DEFINE(HAVE_TERMCAP_LINUX))
])
AC_DEFUN(AC_GTKTTY_REQUIREMENTS,
[
	dnl Checks for header files.
	AC_HEADER_SYS_WAIT
	AC_CHECK_HEADERS(fcntl.h sys/ioctl.h unistd.h)

	dnl Check for optional functions.
	AC_CHECK_FUNCS(getdtablesize)
	AC_CHECK_FUNCS(getrlimit)
	AC_CHECK_FUNC(wait4,[
		AC_DEFINE(HAVE_WAIT4)
		HAVE_WAIT4=yes
	])
	
	dnl Check for required functions.
	MC_IF_VAR_EQ(HAVE_WAIT4, yes,,
		AC_CHECK_FUNC(waitpid,,[AC_MSG_ERROR(Cannot wait for process termination)])
	)
	AC_CHECK_FUNC(tcgetattr,,[AC_MSG_ERROR(Cannot access terminal attributes)])
	AC_CHECK_FUNC(putenv,,[AC_MSG_ERROR(Cannot set environment variables)])

	dnl Type checks.
	AC_TYPE_SIGNAL
])

dnl Setup default CFLAGS value.
MC_IF_VAR_EQ(CFLAGS, "", CFLAGS="-g")
CFLAGS_saved="$CFLAGS"
unset CFLAGS
dnl Checks for compiler characteristics, header files, typedefs and structures.
AC_PROG_CC
MC_STR_CONTAINS($CFLAGS, -g, CFLAGS_include_g=yes)
MC_STR_CONTAINS($CFLAGS, -O, CFLAGS_include_O=yes)
CFLAGS="$CFLAGS_saved"
AC_PROG_CPP
AC_C_CONST
AC_PROG_GCC_TRADITIONAL
AC_C_INLINE
AC_HEADER_STDC

dnl Setup CFLAGS for debugging.
MC_IF_VAR_EQ(enable_debug, yes,
	MC_IF_VAR_EQ(CFLAGS_include_g, yes,
		MC_ADD_TO_VAR(CFLAGS, -g, -g)
	)
	
	MC_IF_VAR_EQ(GCC, yes,
		dnl MC_ADD_TO_VAR(CFLAGS, -fvolatile-global, -fvolatile-global)
		dnl MC_ADD_TO_VAR(CFLAGS, -fverbose-asm, -fverbose-asm)
	)
)

dnl Further setup CFLAGS for GCC.
MC_IF_VAR_EQ(GCC, yes,
        	
	dnl Warnings.
	MC_ADD_TO_VAR(CFLAGS, -Wall, -Wall)
	MC_ADD_TO_VAR(CFLAGS, -Wmissing-prototypes, -Wmissing-prototypes)
	MC_ADD_TO_VAR(CFLAGS, -Wstrict-prototypes, -Wstrict-prototypes)
	MC_ADD_TO_VAR(CFLAGS, -Winline, -Winline)
	MC_ADD_TO_VAR(CFLAGS, -Wpointer-arith, -Wpointer-arith)
	MC_IF_VAR_EQ(enable_pedantic_ansi, yes,
		MC_ADD_TO_VAR(CFLAGS, -ansi, -ansi)
		MC_ADD_TO_VAR(CFLAGS, -pedantic, -pedantic)
	)

	dnl Optimizations
	MC_ADD_TO_VAR(CFLAGS, -O, -O6)
	MC_ADD_TO_VAR(CFLAGS, -fstrength-reduce, -fstrength-reduce)
	MC_ADD_TO_VAR(CFLAGS, -fexpensive-optimizations, -fexpensive-optimizations)
	MC_ADD_TO_VAR(CFLAGS, -finline-functions, -finline-functions)
	MC_ADD_TO_VAR(CFLAGS, -frerun-cse-after-loop, -frerun-cse-after-loop)
	MC_ADD_TO_VAR(CFLAGS, -freg-struct-return, -freg-struct-return)
	MC_ADD_TO_VAR(CFLAGS, -fnonnull-objects, -fnonnull-objects)
	dnl use of -pipe breaks most gcc setups that aren't using GNU binutils
	dnl MC_ADD_TO_VAR(CFLAGS, -pipe, -pipe)
	dnl -funroll-loops gives problems with -O and templates (see Rep-CppBug_1.C)
	dnl MC_ADD_TO_VAR(CFLAGS, -funroll-loops, -funroll-loops)
	dnl MC_ADD_TO_VAR(CFLAGS, -fhandle-signatures, -fhandle-signatures)
	dnl MC_ADD_TO_VAR(CFLAGS, -fhandle-exceptions, -fhandle-exceptions)
	dnl MC_ADD_TO_VAR(CFLAGS, -frtti, -frtti)
,
	MC_IF_VAR_EQ(CFLAGS_include_O, yes,
		MC_ADD_TO_VAR(CFLAGS, -O, -O2)
	)
)

dnl Checks for programs.
AC_PROG_INSTALL
AC_PROG_LN_S
AM_PROG_LIBTOOL

dnl Check for package requirements.

AC_LIBGTKTTY_REQUIREMENTS
AC_GTKLED_REQUIREMENTS
AC_GTKTERM_REQUIREMENTS
AC_GTKTTY_REQUIREMENTS
AC_GTKVTEMU_REQUIREMENTS
AC_GTKVT102_REQUIREMENTS
AC_DEFINE_UNQUOTED(LIBGTKTTY_MAJOR, $LIBGTKTTY_MAJOR)
AC_DEFINE_UNQUOTED(LIBGTKTTY_REVISION, $LIBGTKTTY_REVISION)
AC_DEFINE_UNQUOTED(LIBGTKTTY_AGE, $LIBGTKTTY_AGE)
AC_DEFINE_UNQUOTED(LIBGTKTTY_VERSION, "$LIBGTKTTY_VERSION")

dnl Automake @VARIABLE@ exports.
AC_SUBST(CFLAGS)
AC_SUBST(CPPFLAGS)
AC_SUBST(LDFLAGS)
AC_SUBST(LIBGTKTTY_MAJOR)
AC_SUBST(LIBGTKTTY_REVISION)
AC_SUBST(LIBGTKTTY_AGE)
AC_SUBST(LIBGTKTTY_VERSION)

dnl Create files.
AC_OUTPUT([
Makefile 
])
