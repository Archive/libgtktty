/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * LibGtkTty: Terminal emulation widgets
 * Copyright (C) 1997 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include	"config.h"

#define	__LIBGTKTTY_OMIT_INCLUDES__
#include	"libgtktty.h"


const guint     gtktty_libversion_major = LIBGTKTTY_MAJOR;
const guint     gtktty_libversion_revision = LIBGTKTTY_REVISION;
const guint     gtktty_libversion_age = LIBGTKTTY_AGE;
const gchar     *gtktty_libversion = LIBGTKTTY_VERSION;
